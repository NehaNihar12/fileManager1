import {
  Editable,
  EditableInput,
  EditablePreview,
  propNames,
} from "@chakra-ui/react";

function EditableEx() {
  /* Here's a custom control */

  return (
    <Editable
      defaultValue="Take some chakra"
      onSubmit={(s) => {
        console.log(s);
      }}
      isPreviewFocusable={propNames.canEdit}
    >
      <EditablePreview />
      <EditableInput />
    </Editable>
  );
}

export default EditableEx;
