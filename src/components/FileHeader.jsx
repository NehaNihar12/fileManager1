import React, { Component } from "react";

import {
  ButtonGroup,
  IconButton,
  Tooltip,
  Input,
  List,
  ListItem,
} from "@chakra-ui/react";
import { VscNewFile, VscNewFolder, VscTrash, VscEdit } from "react-icons/vsc";

import File from "./File";
import Folder from "./Folder";

class FileHeader extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: "",
      activeButton: this.props.activeButton,
    };
  }

  //clicking the navbar buttons
  handleActiveButton = (e) => {
    //e.preventDefault();
    console.log(e.target.value);
    this.setState({ activeButton: e.target.parentElement.value });
    this.props.handleActiveButton(e.target.parentElement.value);
  };

  //Rename active file if selected button is rename
  renameItem = (name) => {
    this.props.renameItem(name);
    //   const newdir = this.state.fileDirectory.filter((obj) => {
    //     if (obj.active === true) {
    //       obj.name = name;
    //       //obj.active = false;
    //     }
    //     return obj;
    //   });
    //   this.setState({ fileDirectory: [...newdir], activeButton: "" }, () => {
    //     localStorage.setItem(
    //       "directory",
    //       JSON.stringify(this.state.fileDirectory)
    //     );
    //   });
  };

  handleDelete = () => {
    this.props.handleDelete();
  };

  //controlled input element
  handleChange = (e) => {
    this.setState({ value: e.target.value });
  };

  //on submitting new filename
  handleSubmit = (e) => {
    e.preventDefault();
    if (this.state.value !== "") {
      if (
        this.state.activeButton === "newFile" ||
        this.state.activeButton === "newFolder"
      ) {
        this.props.handleAddNewFile(this.state.value);
      }
      this.setState({ value: "" });
    }
  };

  //file that is active
  handleActiveFile = (obj) => {
    this.props.handleActiveFile(obj.id);
  };

  render() {
    const { fileDir, activeButton, activeFileId } = this.props;
    console.log(activeButton);

    return (
      <div>
        <ButtonGroup justifyvalue="center" size="sm" pt="6px" pb="3px" px="0">
          <Tooltip hasArrow label="New File" bg="red.600">
            <IconButton
              size="sm"
              icon={<VscNewFile onClick={this.handleActiveButton} />}
              value="newFile"
            />
          </Tooltip>
          <Tooltip hasArrow label="New Folder" bg="red.600">
            <IconButton
              size="sm"
              icon={<VscNewFolder onClick={this.handleActiveButton} />}
              value="newFolder"
            />
          </Tooltip>
          <Tooltip hasArrow label="Delete" bg="red.600">
            <IconButton
              size="sm"
              icon={<VscTrash onClick={this.handleDelete} />}
              value="Delete"
            />
          </Tooltip>
          <Tooltip hasArrow label="Rename" bg="red.600">
            <IconButton
              size="sm"
              ml="auto"
              icon={<VscEdit onClick={this.handleActiveButton} />}
              value="Rename"
            />
          </Tooltip>
        </ButtonGroup>
        {this.state.activeButton === "newFile" ||
        this.state.activeButton === "newFolder" ? (
          <form onSubmit={this.handleSubmit}>
            <Input
              value={this.state.value}
              onChange={this.handleChange}
              placeholder="new name..."
              size="sm"
              mr="2rem"
            />
          </form>
        ) : null}
        <List spacing={3}>
          {fileDir.map((obj) =>
            obj.type === "newFile" ? (
              <ListItem
                className={obj.id === activeFileId ? "list fillActive" : "list"}
              >
                <File
                  file={obj}
                  handleActiveFile={this.handleActiveFile}
                  activeButton={activeButton}
                  renameItem={this.renameItem}
                ></File>
              </ListItem>
            ) : (
              <ListItem
                className={obj.id === activeFileId ? "list fillActive" : "list"}
              >
                <Folder
                  folder={obj}
                  handleActiveFile={this.handleActiveFile}
                  activeButton={activeButton}
                  renameItem={this.renameItem}
                ></Folder>
              </ListItem>
            )
          )}
        </List>
      </div>
    );
  }
}

export default FileHeader;
