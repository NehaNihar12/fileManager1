import React, { Component } from "react";
import { v4 as uuidv4 } from "uuid";

import "./App.css";
import FileDrawer from "./components/FileDrawer";
import Editor from "./container/Editor";

class App extends Component {
  constructor() {
    super();
    this.state = {
      fileDir: [
        {
          key: uuidv4(),
          id: uuidv4(),
          name: "hello world",
          content: "hello",
          createdAt: new Date(),
          type: "file",
        },
      ],
      activeFileId: null,
      activeButton: "",
      value: "*hello*",
    };
  }

  //mounting files in the local storage
  componentDidMount() {
    this.getDirectory();
  }

  getDirectory() {
    const state = JSON.parse(window.localStorage.getItem("directory"));
    if (state) {
      this.setState({
        fileDir: state,
      });
    }
  }

  //set the id of a file that is active
  handleActiveFile = (id) => {
    this.setState({ activeFileId: id }, () => {
      const activeFile = this.state.fileDir.filter(
        (obj) => obj.id === this.state.activeFileId
      );
      if (activeFile.length !== 0) {
        this.setState({ value: activeFile[0].content });
      }
    });
  };

  //set the active button
  handleActiveButton = (button) => {
    this.setState({ activeButton: button });
  };

  //rename item
  renameItem = (name) => {
    const newdir = this.state.fileDir.filter((obj) => {
      if (obj.id === this.state.activeFileId) {
        obj.name = name;
      }
      return obj;
    });
    this.setState({ fileDir: [...newdir], activeButton: "" }, () => {
      localStorage.setItem("directory", JSON.stringify(this.state.fileDir));
    });
  };

  //delete item

  handleDelete = () => {
    this.setState(
      {
        fileDir: this.state.fileDir.filter(
          (obj) => obj.id !== this.state.activeFileId
        ),
      },
      () => {
        this.setState({ activeButton: "" });
        localStorage.setItem("directory", JSON.stringify(this.state.fileDir));
      }
    );
  };

  //add a new file/folder to directory
  handleAddNewFile = (name) => {
    this.setState(
      {
        fileDir: [
          {
            key: uuidv4(),
            id: uuidv4(),
            name: name,
            content: "hi",
            createdAt: new Date(),
            type: this.state.activeButton,
          },
          ...this.state.fileDir,
        ],
      },
      () => {
        this.setState({ activeButton: "" });
        // console.log(this.state.fileDir);
        localStorage.setItem("directory", JSON.stringify(this.state.fileDir));
      }
    );

    this.setState({ activeButton: "" });
  };

  handleEditFile = (value) => {
    console.log(value);
    this.setState({ value: value });
    const newDir = this.state.fileDir.map((obj) => {
      if (obj.id === this.state.activeFileId) {
        obj = { ...obj, content: value };
      }
      return obj;
    });

    this.setState(
      {
        fileDir: [...newDir],
      },
      () => {
        localStorage.setItem("directory", JSON.stringify(this.state.fileDir));
      }
    );
  };

  render() {
    console.log(this.state.activeButton);
    return (
      <div>
        <div
          style={{
            display: "flex",
            flexDirection: "row",
            flexFlow: "row wrap",
          }}
        >
          <FileDrawer
            activeButton={this.state.activeButton}
            fileDir={this.state.fileDir}
            activeFileId={this.state.activeFileId}
            handleAddNewFile={this.handleAddNewFile}
            handleActiveButton={this.handleActiveButton}
            handleActiveFile={this.handleActiveFile}
            handleDelete={this.handleDelete}
            renameItem={this.renameItem}
          ></FileDrawer>

          <Editor
            value={this.state.value}
            handleEditFile={this.handleEditFile}
          ></Editor>
        </div>
      </div>
    );
  }
}

export default App;
