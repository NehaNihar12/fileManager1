import { React, Component } from "react";
import MDEditor from "@uiw/react-md-editor";

class Editor extends Component {
  render() {
    return (
      <div className="container">
        <MDEditor
          value={this.props.value}
          //onChange={this.setValue}
          onChange={this.props.handleEditFile}
        />
        <MDEditor.Markdown />
      </div>
    );
  }
}

export default Editor;
